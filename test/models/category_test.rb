require 'test_helper'

class CategoryTest < ActiveSupport::TestCase
  test "subcategory" do
    c1 = categories(:c1)
    c2 = categories(:c2)
    c22 = c1.create_subcategory
    c3 = c2.create_subcategory

    assert_includes c1.subcategories, c2
    assert_includes c1.subcategories, c22
    assert_includes c2.subcategories, c3

    assert c2.parent == c1
    assert c22.parent == c1
    assert c3.parent == c2
    assert c3.parent != c22
  end
end
