require 'test_helper'

class ServiceTest < ActiveSupport::TestCase
  test "services_categories" do
    service1 = services(:service1)
    service2 = services(:service2)
    service3 = services(:service3)

    c1 = categories(:c1)
    c2 = categories(:c2)

    c1.services << [service1, service3]
    c2.services << [service2, service3]

    assert_includes service1.categories, c1
    assert_includes service2.categories, c2
    assert_includes service3.categories, c1
    assert_includes service3.categories, c2
  end
end
