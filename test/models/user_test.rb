require 'test_helper'

class UserTest < ActiveSupport::TestCase
  test "user_doer_favorite_relationship" do
    u1 = users(:u1)
    u2 = users(:u2)
    u3 = users(:u3)

    u1.favorite_doers << u3.doer
    u2.favorite_doers << u3.doer

    assert_includes u1.favorite_doers, u3.doer
    assert_includes u2.favorite_doers, u3.doer
    assert_includes u3.doer.favorite_ins, u1
    assert_includes u3.doer.favorite_ins, u2
  end
end
