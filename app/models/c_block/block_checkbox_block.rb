class CBlock::BlockCheckboxBlock < ActiveRecord::Base
  acts_as :calculation_block

  belongs_to :page, class_name: 'CalculationPage'
end
