class Category < ActiveRecord::Base
  has_many :subcategories, class_name: 'Category', foreign_key: 'parent_id'
  belongs_to :parent, class_name: 'Category'

  has_and_belongs_to_many :services

  def create_subcategory(attributes={})
    new_category = Category.create attributes
    self.subcategories << new_category
    new_category
  end
end
