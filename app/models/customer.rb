class Customer < ActiveRecord::Base
  belongs_to :user

  belongs_to :city

  has_many :orders

  has_many :messages

  has_many :feedbacks

  has_many :contacts, as: :subject
end
