class Message < ActiveRecord::Base
  belongs_to :customer

  belongs_to :doer

  belongs_to :order
end
