# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150903105356) do

  create_table "c_block_block_checkbox_blocks", force: :cascade do |t|
    t.integer  "page_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "c_block_block_checkboxes", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "c_block_block_counts", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "c_block_block_droplists", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "c_block_block_images", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "c_block_block_labels", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "c_block_block_textareas", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "c_block_block_types", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "calculation_blocks", force: :cascade do |t|
    t.integer  "actable_id"
    t.string   "actable_type"
    t.integer  "calculation_page_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  create_table "calculation_data", force: :cascade do |t|
    t.integer  "calculation_block_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "calculation_pages", force: :cascade do |t|
    t.integer  "service_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "categories", force: :cascade do |t|
    t.string   "name"
    t.boolean  "active"
    t.integer  "parent_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "categories", ["parent_id"], name: "index_categories_on_parent_id"

  create_table "categories_services", id: false, force: :cascade do |t|
    t.integer "category_id", null: false
    t.integer "service_id",  null: false
  end

  add_index "categories_services", ["category_id"], name: "index_categories_services_on_category_id"
  add_index "categories_services", ["service_id"], name: "index_categories_services_on_service_id"

  create_table "cities", force: :cascade do |t|
    t.integer  "country_id"
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "contacts", force: :cascade do |t|
    t.string   "type"
    t.string   "data"
    t.integer  "subject_id"
    t.string   "subject_type"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "contacts", ["subject_id"], name: "index_contacts_on_subject_id"

  create_table "countries", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "customers", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "city_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "doer_companies", force: :cascade do |t|
    t.string   "name"
    t.string   "company_type"
    t.date     "founding_date"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "doer_individuals", force: :cascade do |t|
    t.string   "first_name"
    t.string   "middle_name"
    t.string   "last_name"
    t.date     "birth_date"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "doers", force: :cascade do |t|
    t.integer  "actable_id"
    t.string   "actable_type"
    t.integer  "user_id"
    t.integer  "city_id"
    t.integer  "feedbacks_counter"
    t.integer  "feedbacks_positive_counter"
    t.integer  "feedbacks_negative_counter"
    t.decimal  "rating"
    t.boolean  "avalible"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "doers_services", id: false, force: :cascade do |t|
    t.integer "service_id", null: false
    t.integer "doer_id",    null: false
  end

  add_index "doers_services", ["doer_id"], name: "index_doers_services_on_doer_id"
  add_index "doers_services", ["service_id"], name: "index_doers_services_on_service_id"

  create_table "favorite_relationships", id: false, force: :cascade do |t|
    t.integer "user_id"
    t.integer "doer_id"
  end

  add_index "favorite_relationships", ["doer_id", "user_id"], name: "index_favorite_relationships_on_doer_id_and_user_id", unique: true
  add_index "favorite_relationships", ["user_id", "doer_id"], name: "index_favorite_relationships_on_user_id_and_doer_id", unique: true

  create_table "feedbacks", force: :cascade do |t|
    t.integer  "order_id"
    t.integer  "doer_id"
    t.integer  "customer_id"
    t.boolean  "active",                  default: true
    t.boolean  "edited",                  default: false
    t.integer  "rating",                                  null: false
    t.string   "comment",     limit: 512
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
  end

  create_table "messages", force: :cascade do |t|
    t.integer  "customer_id"
    t.integer  "doer_id"
    t.integer  "order_id"
    t.boolean  "was_read",    default: false, null: false
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "open_orders", force: :cascade do |t|
    t.integer  "order_id"
    t.integer  "doer_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "order_data", force: :cascade do |t|
    t.integer  "order_id"
    t.integer  "calculation_block_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "order_images", force: :cascade do |t|
    t.integer  "order_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "orders", force: :cascade do |t|
    t.integer  "service_id"
    t.integer  "customer_id"
    t.integer  "doer_id"
    t.string   "cid"
    t.boolean  "doer_accepted",     default: false
    t.boolean  "customer_accepted", default: false
    t.datetime "date_start",                        null: false
    t.datetime "date_end"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
  end

  create_table "payments", force: :cascade do |t|
    t.integer  "doer_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "services", force: :cascade do |t|
    t.string   "name"
    t.boolean  "active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "system_messages", force: :cascade do |t|
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.integer  "doer_id"
    t.integer  "customer_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

end
