class CreateServiceDoer < ActiveRecord::Migration
  def change
    create_join_table :services, :doers do |t|
      t.index :service_id
      t.index :doer_id
    end
  end
end
