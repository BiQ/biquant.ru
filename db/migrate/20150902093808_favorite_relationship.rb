class FavoriteRelationship < ActiveRecord::Migration
  def up
    create_table :favorite_relationships, id: false do |t|
      t.integer :user_id
      t.integer :doer_id
    end

    add_index :favorite_relationships, [:user_id, :doer_id], :unique => true
    add_index :favorite_relationships, [:doer_id, :user_id], :unique => true
  end

  def down
    remove_index :favorite_relationships, [:user_id, :doer_id]
    remove_index :favorite_relationships, [:doer_id, :user_id]
    drop_table :favorite_relationships
  end
end
