class CreateFeedbacks < ActiveRecord::Migration
  def change
    create_table :feedbacks do |t|
      t.belongs_to :order
      t.belongs_to :doer
      t.belongs_to :customer

      t.boolean :active, default: true
      t.boolean :edited, default: false
      t.integer :rating, null: false
      t.string :comment, limit: 512

      t.timestamps null: false
    end
  end
end
