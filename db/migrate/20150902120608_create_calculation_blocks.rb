class CreateCalculationBlocks < ActiveRecord::Migration
  def change
    create_table :calculation_blocks do |t|
      t.actable
      t.belongs_to :calculation_page
      t.timestamps null: false
    end
  end
end
