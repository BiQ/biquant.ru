class CreateOpenOrders < ActiveRecord::Migration
  def change
    create_table :open_orders do |t|
      t.belongs_to :order
      t.belongs_to :doer

      t.timestamps null: false
    end
  end
end
