class CreateOrderData < ActiveRecord::Migration
  def change
    create_table :order_data do |t|
      t.belongs_to :order
      t.belongs_to :calculation_block
      t.timestamps null: false
    end
  end
end
