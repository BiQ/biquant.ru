class CreateDoerCompanies < ActiveRecord::Migration
  def change
    create_table :doer_companies do |t|
      t.string :name
      t.string :company_type
      t.date :founding_date

      t.timestamps null: false
    end
  end
end
