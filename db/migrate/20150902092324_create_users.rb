class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.belongs_to :doer
      t.belongs_to :customer
      t.timestamps null: false
    end
  end
end
