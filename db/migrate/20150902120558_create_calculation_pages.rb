class CreateCalculationPages < ActiveRecord::Migration
  def change
    create_table :calculation_pages do |t|
      t.belongs_to :service
      t.timestamps null: false
    end
  end
end
