class CreateContacts < ActiveRecord::Migration
  def change
    create_table :contacts do |t|
      t.string :type
      t.string :data
      t.integer :subject_id
      t.string :subject_type

      t.index :subject_id

      t.timestamps null: false
    end
  end
end
